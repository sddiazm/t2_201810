package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {


		
	private String taxi_id;
	
	private String trip_id;
	
	private int trip_seconds;
	
	private double trip_miles;
	
	private double trip_total;
	
	private int comunityArea;
	
	
	
	public Service (String pTaxiId, String pTripId, int pTripSeconds, double pTripMiles, double pTripTotal , int pComunityArea){
		taxi_id= pTaxiId;
		trip_id=pTripId;
		trip_seconds = pTripSeconds;
		trip_miles = pTripMiles;
		trip_total= pTripTotal;
		comunityArea= pComunityArea;
	}
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return trip_id;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi_id;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return trip_seconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return trip_miles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return trip_total;
	}
	
	public int comunityArea(){
		return comunityArea;
	}

	@Override
	public int compareTo(Service o) {
		return trip_id.compareTo(o.getTripId());
	}
}

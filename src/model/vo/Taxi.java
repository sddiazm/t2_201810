package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{
	
	
	private String taxi_id;

	private String company;


	
	public Taxi(String pTaxiId, String pCompany){
		taxi_id = pTaxiId;
		company = pCompany;
	}
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId( ) {
		// TODO Auto-generated method stub
		return taxi_id;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}
	
	@Override
	public int compareTo(Taxi o) {
		return taxi_id.compareToIgnoreCase(o.getTaxiId());
	}	
}

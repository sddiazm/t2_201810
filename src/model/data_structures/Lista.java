package model.data_structures;

public class Lista<T extends Comparable<T>> implements LinkedList<T> {

	private Node primero;

	private int tamanio;

	private Node actual;

	public Lista(){
		primero = null;
		tamanio=0;
		actual=null;
	}

	@Override
	public void add(T pElement) {
		// TODO Auto-generated method stub
		Node elemento = new Node(pElement);
		if(primero==null){
			primero= elemento;
			tamanio++;
		}
		else if(primero!=null){
			Node ultimo = buscarUltimo();
			ultimo.cambiarSiguiente(elemento);
			ultimo.darSiguiente().cambiarAnterior(ultimo);
			tamanio++;
		}


	}

	public void addNoRepetido(T pElement){
		Node elemento = new Node(pElement);
		if(primero==null){
			primero= elemento;
			tamanio++;
		}
		else if(primero!=null && primero.darElemento().compareTo(pElement)!=0){
			Node actual = primero;
			boolean centinela= true;
			while(actual.darSiguiente()!=null)
			{
				if(actual.darElemento().compareTo(pElement)==0)
				{
					centinela= false;
					break;
				}
				actual= actual.darSiguiente();
			}
			if(centinela){
				Node ultimo = actual;
				ultimo.cambiarSiguiente(elemento);
				ultimo.darSiguiente().cambiarAnterior(ultimo);
				tamanio++;
			}
		}
	}







	@Override
	public void delete(T pElement) {
		// TODO Auto-generated method stub
		Node buscado=primero;
		Node ultimo = buscarUltimo();
		if(buscado.darElemento().compareTo(pElement)==0){
			primero=primero.darSiguiente();
			tamanio--;
		}
		else if(ultimo.darElemento().compareTo(pElement)==0)
		{
			Node anterior= ultimo.darAnterior();
			anterior.cambiarSiguiente(null);
			tamanio--;
		}
		else{
			while(buscado.darElemento().compareTo(pElement)!=0 && buscado.darSiguiente()!=null){
				buscado= buscado.darSiguiente();
			}
			if(buscado.darElemento().compareTo(pElement)==0)
			{
				Node anterior = buscado.darAnterior();
				Node siguiente = buscado.darSiguiente();
				anterior.cambiarSiguiente(siguiente);
				siguiente.cambiarAnterior(anterior);
				tamanio--;
			}
		}
	}

	@Override
	public T get(T pElement) {
		// TODO Auto-generated method stub
		Node buscado=primero;
		while(buscado.darElemento().compareTo(pElement)!=0){
			buscado= buscado.darSiguiente();
		}
		if(buscado.darElemento().compareTo(pElement)==0)
		{
			return (T) buscado.darElemento();
		}
		return null;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return tamanio;
	}

	@Override
	public T get(int pPosition) {
		// TODO Auto-generated method stub
		if(tamanio==0){
			return null;
		}
		int contador = 1;
		Node actual = primero;
		while(contador!=pPosition && actual.darSiguiente()!=null){
			actual = actual.darSiguiente();
			contador++;
		}
		return actual.darElemento();
	}

	@Override
	public Node Listing() {
		// TODO Auto-generated method stub
		if(primero!= null)
		{
			return primero;
		}
		else
			return null;
	}

	@Override
	public T getCurrent() {
		if(primero!= null){
			return actual.darElemento();
		}
		else
			return null;
	}

	@Override
	public T next() {
		if(actual== null)
		{
			actual= primero;
		}
		else 
			actual = actual.darSiguiente();
		return actual.darElemento();
	}

	public boolean hasNext(){
		boolean rta =false;
		if(actual.darSiguiente()!=null)
			return true;
		return rta;
	}


	public Node buscarUltimo(){
		Node actual = primero;
		while(actual.darSiguiente()!=null){
			actual = actual.darSiguiente();
		}
		return actual;
	}


	public class Node  {

		private T elemento;

		private Node siguiente;

		private Node anterior;

		public Node(T t){
			elemento = t;
		}

		public Node darSiguiente(){
			return siguiente;
		}

		public Node darAnterior(){
			return anterior;
		}

		public T darElemento(){
			return elemento;
		}

		public void cambiarSiguiente(Node pNode){
			this.siguiente= pNode;
		}

		public void cambiarAnterior(Node pAnterior){
			this.anterior = pAnterior;
		}
	}



}

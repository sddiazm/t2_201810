package model.logic;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.LinkedList;
import model.data_structures.Lista;
import model.data_structures.Lista.Node;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;


public class TaxiTripsManager implements ITaxiTripsManager {


	private int contador;

	private Lista<Service> servicios;

	private Lista<Taxi> taxis;

	// TODO
	// Definition of data model 

	public void loadServices (String serviceFile) {

		JsonParser parser = new JsonParser();
		servicios = new Lista<Service>();
		taxis = new Lista<Taxi>();

		try {

			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
			JsonArray arr= (JsonArray) parser.parse(new FileReader(serviceFile));

			/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
			for (int i = 0; arr != null && i < arr.size(); i++)
			{
				JsonObject obj= (JsonObject) arr.get(i);


				/* Obtener la propiedad company de un servicio ( String ) */
				String company= "NaN";
				if(obj.get("company") != null){
					company = obj.get("company").getAsString();
				}

				/* Obtener la propiedad comunity Area de un servicio ( int ) */
				int comunityArea= 0;
				if(obj.get("dropoff_community_area") != null){
					comunityArea = obj.get("dropoff_community_area").getAsInt();
				}
				
				
				/* Obtener la propiedad taxi_id de un servicio ( String )*/
				String taxi_id= "NaN";
				if(obj.get("taxi_id") != null){
					taxi_id = obj.get("taxi_id").getAsString();
				}

				/* Obtener la propiedad trip_id de un servicio ( String ) */
				String trip_id =obj.get("trip_id").getAsString() != null ? obj.get("trip_id").getAsString() : "NaN";

				/* Obtener la propiedad tripSeconds de un servicio ( int ) */
				int trip_seconds =obj.get("trip_seconds").getAsInt() != 0 ? obj.get("trip_seconds").getAsInt() : 0;

				/* Obtener la propiedad tripMiles de un servicio ( double ) */
				double trip_miles =obj.get("trip_miles").getAsDouble() != 0 ? obj.get("trip_miles").getAsDouble() : 0.0;

				/* Obtener la propiedad tripTotal de un servicio ( double ) */
				double trip_total =obj.get("trip_total").getAsDouble() != 0 ? obj.get("trip_total").getAsDouble() : 0.0;

				Service servicio = new Service(taxi_id, trip_id, trip_seconds, trip_miles, trip_total, comunityArea);
				contador++;
				Taxi taxi = new Taxi(taxi_id, company);
				servicios.add(servicio);
				taxis.add(taxi);
			}
		}
		catch (JsonIOException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}

		System.out.println("Inside loadServices with " + serviceFile);
	}

	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company) {
		Lista<Taxi> taxisMismaCompa˝ia = new Lista<Taxi>();

		for (int i = 0; i < taxis.size(); i++) {
			Taxi actual = taxis.get(i);
			if(actual.getCompany().equals(company)){
				taxisMismaCompa˝ia.addNoRepetido(actual);
			}
		}
		return taxisMismaCompa˝ia;
	}

	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {

		Lista<Service> mismaArea = new Lista<Service>();
		for (int i = 0; i < servicios.size(); i++) {
			Service actual = servicios.get(i);
			if(actual.comunityArea()==communityArea){
				mismaArea.add(actual);
			}
			
		}
//		System.out.println("Inside getTaxiServicesToCommunityArea with " + communityArea);
		return mismaArea;
	}


}
